VUE3 迁移指南<br>
一、VUE 新特性<br>
Vue 3 中需要关注的一些新特性包括:<br>
组合式 API*<br>
单文件组件中的组合式 API 语法糖 (<script setup>)*<br>
Teleport 组件<br>
Fragments 片段<br>
Emits 组件选项\*_<br>
来自 @vue/runtime-core 的 createRenderer API 用来创建自定义渲染函数<br>
单文件组件中的状态驱动的 CSS 变量 (<style> 中的 v-bind)_<br>
SFC <style scoped> 新增全局规则和针对插槽内容的规则<br>
Suspense 实验性<br>
具体深入了解，可登录 vue 官网进行系统学习。<br>
网址：https://v3-migration.vuejs.org/zh/<br>
二、VUE3 相比 VUE2 有哪些优点与改变 <br>
1. diff 算法的提升<br>
vue2 中的虚拟 DOM 是全量的对比，也就是不管是写死的还是动态节点都会一层层比较，浪费时间在静态节点上。<br>
vue3 新增静态标记（patchflag ），与之前虚拟节点对比，只对比带有 patch flag 的节点，可通过 flag 信息得知当前节点要对比的具体内容。 <br>
2. 静态提升<br>
vue2 不管是否参与更新，都会重新创建再渲染。<br>
vue3 对于不参与更新的元素，会做静态提升，只被创建一次，在渲染时直接复用即可。<br>
3、事件侦听器缓存。<br>
4、 ssr 渲染<br>
5、更好的 ts 支持<br>
6、Composition Api<br>
7、更先进的组件<br>
vue2 不允许 template 下写两个组件，vue3 允许，将为我们创建一个虚拟的 Fragment 节点。<br>
8、自定义渲染 api<br>
9、按需编译，体积比 vue2 更小 <br>
10.支持多根节点组件 <br>
11. 响应式原理不同<br>
vue2 实现双向数据绑定原理，是通过 ES5 的 Object.defineProperty,根据具体的 key 去读取和修改。其中的 setter 方法来实现数据劫持，getter 实现数据修改。但是必须要先知道拦截和修改的 key，<br>所以 vue2 对于新增的属性无能为力，比如无法监听属性的新增和删除，数组索引和长度的变更，解决方法使用 Vue.set(object, properName,value)等嵌套对象添加响应式。<br>
在 vue3 中使用 es5 中得更快的 proxy，替代了 Object.defineProperty。proxy 可以理解为在对象外加了一层拦截，任何人要访问该对象，都要通过这层拦截。且 proxy 直接对对象拦截而非属性，并返回一个对象，具有更好的响应式支持。 
12.生命周期变化<br>

14. mixins 更改<br>
15. 父子传值的变化<br>
16. 碎片化节点<br>
三、所需电脑配置<br>
由于 vue3 基于 node 版本在 V14 及以上的版本，node 在 14 以上的版本需要至少 win10 以上系统。电脑内存至少 8G。<br>
四、新的版本框架推荐<br>
Vue 3 的支持库进行了重大更新。以下是新的默认建议的摘要: <br>
1.新版本的 Router, Devtools & test utils 来支持 Vue 3 <br>
2.构建工具链: Vue CLI -> Vite <br>
3.状态管理: Vuex -> Pinia<br>
4.IDE 支持: Vetur -> Volar <br>
5.新的 TypeScript 命令行工具: vue-tsc <br>
6.静态网站生成: VuePress -> VitePress<br>
7.JSX: @vue/babel-preset-jsx -> @vue/babel-plugin-jsx<br>
考虑到我们现有框架使用习惯，可采用 1,2,3,4,5 这几点。<br>
五、代码质量及风格检测插件<br>
代码质量检测 eslint:<br>
代码风格检测 prettier:<br>
两个插件配置，后面会形成统一的标准配置文档，所有项目引用使用同一个配置。保证代码风格基本一致。<br>
六、VUE3 相关框架统计<br>
1.element-plus;<br>
2.vant3/4 支持 vue3.0 版本； <br>
3. axios 网络请求；<br>
4.vuex 状态管理； <br>
5. Pinia->vue3 新提供的一套状态管理可替代 vuex；<br>
6.eslint->js/ts 代码质量检测工具；<br>
7.prettier->代码美化工具；<br>
8.sass、sass-loader sass 样式；<br>
9.less、less-loader less 样式；<br>
10. postcss-px-to-viewport 分辨率适配插件；<br>
11. vue-i18n 国际化插件； <br>
12. js-cookie、@types/js-cookie cookie 管理插件；<br>
13.vite 编译、调试、打包工具->官方推荐替代 webpack；<br>
14.typeScript->vue3 推荐开发语言；<br>
15.echarts->可视化图表库；<br>
16. lodash->工具库，一些防抖、限流、克隆函数等；<br>
17.vue-router->路由插件 <br>
18. moment->日期插件替代使用传统 new Date(),方便易维护；<br>
19.vue-pdf->pdf 加载插件； 20.其他一些打印、地图插件等等<br>
以上都是一些市面主流插件框架，可按需使用。<br>

七、学习指南
1.vue3 学习指南<br>
https://v3-migration.vuejs.org/zh/<br>
2.typescript 学习指南<br>
http://ts.xcatliu.com/introduction/what-is-typescript.html<br>
3.element-plus 学习指南<br>
https://element-plus.gitee.io/zh-CN/<br>
4.vant3/4 学习指南<br>
https://vant-ui.github.io/vant/#/zh-CN<br>
5.es6-阮一峰<br>
https://es6.ruanyifeng.com/<br>
