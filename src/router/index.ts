import { createRouter, createWebHistory, createWebHashHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'Home',
    component: () => import('@/views/home.vue'),
  },
]

const router = createRouter({
  history: createWebHashHistory(import.meta.env.VITE_VUE_APP_BASE_API),
  routes,
})

export default router
