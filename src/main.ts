import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus'
import { AppModule } from '@/store/modules/app'
import '@/style/app_global.scss'
import '@/style/transition.scss'
import '@/style/mixins/var.scss'
import 'element-plus/dist/index.css'
// import "@/style/theme/element-variables.scss";
// import "@/style/theme/primary/index.css";
import LeoDxUi from 'dx-we-ui'
import 'dx-we-ui/lib/index.full.css'
import i18n from '@/lang' // 引入国际化插件
import '@/permission'
import ZhLocale from 'element-plus/lib/locale/lang/zh-cn' // 中文
import EhLocale from 'element-plus/lib/locale/lang/en' // 英文
import * as Icons from '@element-plus/icons-vue'
import {
  ElPagination,
  ElSwitch,
  ElTable,
  ElTableColumn,
  ElPopover,
  ElForm,
  ElRow,
  ElSelect,
  ElCascader,
  ElFormItem,
  ElInput,
  ElButton,
  ElColorPicker,
  ElTree,
  ElPopconfirm,
  ElIcon,
} from 'element-plus' // 引入element组件库
const components = [
  ElPagination,
  ElSwitch,
  ElTable,
  ElTableColumn,
  ElPopover,
  ElForm,
  ElRow,
  ElSelect,
  ElCascader,
  ElFormItem,
  ElInput,
  ElButton,
  ElColorPicker,
  ElTree,
  ElPopconfirm,
  ElIcon,
] // 组件数组

const mainApp = createApp(App)

let locale = ''
//控制 element-plus 组件语言
if (i18n.global.fallbackLocale == 'zh') {
  locale = ZhLocale as unknown as string
  AppModule.SetLanguage('zh')
} else {
  locale = EhLocale as unknown as string
  AppModule.SetLanguage('en')
}

components.forEach((component) => {
  mainApp.component(component.name, component)
})

mainApp.use(ElementPlus, {
  locale,
})
console.log(' HI: 11111 ', import.meta.env.VITE_VUE_APP_BASE_API)
// 注册全局组件
Object.keys(Icons).forEach((key) => {
  mainApp.component(key, Icons[key as keyof typeof Icons])
})

mainApp.use(store).use(router).use(i18n).use(LeoDxUi).mount('#app')
