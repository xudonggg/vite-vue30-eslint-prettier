import ChinaArea from './ChinaArea'

const chinaArea = new ChinaArea({ leave: 3, isall: false })

export const getProvinceName = (provinceCode: any) => {
    let areaList: any = chinaArea.chinaData();
    for (let i = 0; i < areaList.length; i++) {
        if (areaList[i].value == provinceCode) {
            return areaList[i].label;
        }
    }
}

export const getCityName = (provinceCode: any, cityCode: any) => {
    let areaList: any = chinaArea.chinaData();
    for (let i = 0; i < areaList.length; i++) {
        if (areaList[i].value == provinceCode) {
            let cityList = areaList[i].children;
            for (let j = 0; j < cityList.length; j++) {
                if (cityList[j].value == cityCode) {
                    return cityList[j].label;
                }
            }
        }
    }
    return '';
}

export const getAreaName = (provinceCode: any, cityCode: any, areaCode: any) => {
    let areaList: any = chinaArea.chinaData();
    for (let i = 0; i < areaList.length; i++) {
        if (areaList[i].value == provinceCode) {
            let cityList = areaList[i].children;
            for (let j = 0; j < cityList.length; j++) {
                if (cityList[j].value == cityCode) {
                    let areaList = cityList[j].children;
                    for (let k = 0; k < areaList.length; k++) {
                        if (areaList[k].value == areaCode) {
                            return areaList[k].label;
                        }
                    }
                }
            }
        }
    }
    return '';
}