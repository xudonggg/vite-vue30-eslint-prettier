export const CUSTOMER_STATUS = [
    { value: '', label: '全部' },
    { value: '1', label: '正常' },
    { value: '0', label: '已停用' }
]

export const CUSTOMER_ACTION_LIST = [
    { value: '', label: '服务延期' },
    { value: '1', label: '重置密码' },
    { value: '2', label: '查看子项目' },
    { value: '3', label: '停用' },
]

export const CUSTOMER_LEVEL = [
    { value: '0', label: 'A' },
    { value: '1', label: 'B' },
    { value: '2', label: 'C' }
]

// 订单状态
export const ORDER_STATUS = [
    { value: '0', label: '待付款' },
    { value: '1', label: '待发货' },
    { value: '2', label: '已发货' },
    { value: '3', label: '已完成' },
    { value: '4', label: '已关闭' }
]

// 订单类型
export const ORDER_TYPE = [
    { value: '0', label: '正常订单' },
    { value: '1', label: '秒杀订单' }
]

// 订单来源
export const ORDER_SOURCE = [
    { value: '0', label: 'PC订单' },
    { value: '1', label: 'app订单' }
]

// 店铺类型
export const STORE_TYPE = [
    { value: 0, label: '西药' },
    { value: 1, label: '中药' },
    { value: 2, label: '中西药' },
    { value: 3, label: '农贸' },
    { value: 4, label: '百货' },
    { value: 5, label: '电器' },
    { value: 6, label: '酒水' },
    { value: 7, label: '服装配饰' },
    { value: 8, label: '母婴用品' },
    { value: 9, label: '数码办公' },
    { value: 10, label: '汽车用品' },
    { value: 11, label: '箱包' }
]