import Cookies from 'js-cookie';

// App
const sidebarStatusKey = 'sidebar_status'
export const getSidebarStatus = () => Cookies.get(sidebarStatusKey)
export const setSidebarStatus = (sidebarStatus: string) => Cookies.set(sidebarStatusKey, sidebarStatus)

const languageKey = 'language'
export const getLanguage = () => Cookies.get(languageKey)
export const setLanguage = (language: string) => Cookies.set(languageKey, language)

const sizeKey = 'size'
export const getSize = () => Cookies.get(sizeKey)
export const setSize = (size: string) => Cookies.set(sizeKey, size)

// User
const tokenKey = 'leo_medicine_operate_access_token'
export const getToken = () => Cookies.get(tokenKey);
export const setToken = (token: string) => Cookies.set(tokenKey, token);
export const removeToken = () => Cookies.remove(tokenKey);
const userNameKey = 'leo_medicine_operate_username';
export const getUserName = () => Cookies.get(userNameKey);
export const setUserName = (username: string) => Cookies.set(userNameKey, username);
export const removeUserName = () => Cookies.remove(userNameKey);
const passwordKey = 'leo_medicine_operate_password';
export const getPassword = () => Cookies.get(passwordKey);
export const setPassword = (password: string) => Cookies.set(passwordKey, password);
export const removePassword = () => Cookies.remove(passwordKey);