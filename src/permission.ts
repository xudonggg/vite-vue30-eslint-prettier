import router from '@/router';
import { ElMessage } from 'element-plus';
import { RouteLocationNormalized } from 'vue-router'
import { UserModule } from '@/store/modules/user'

const whiteList = ['/login']

router.beforeEach(async (to: RouteLocationNormalized, _: RouteLocationNormalized, next: any) => {
  // Start progress bar

  console.log('xxxxxxxxxxxx beforeEach',  to.path);
  // Determine whether the user has logged in
  if (UserModule.token) {
    // console.log('xxxxxxxxxxxx beforeEach11111111111');
    if (to.path === '/login') {
      // If is logged in, redirect to the home page
      // console.log('xxxxxxxxxxxx beforeEach33333333333');
      next({ path: '/' })
      
    } else {
      // Check whether the user has obtained his permission roles
      // console.log('xxxxxxxxxxxx beforeEach44444444444444');
      if (!UserModule.userId || UserModule.userId == '') {
        try {
          await UserModule.GetUserInfo()
          next({ ...to, replace: true })
        } catch (err) {
          UserModule.ResetToken()
          ElMessage.error(err || 'Has Error')
          next(`/login?redirect=${to.path}`)
        }
      } else {
        next()
      }
      // next()
    }
  } else {
    // console.log('xxxxxxxxxxxx beforeEach2222222222');
    // Has no token
    if (whiteList.indexOf(to.path) !== -1) {
      // In the free login whitelist, go directly
      next()
    } else {
      // Other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`)
    
    }
  }
})

router.afterEach((to: RouteLocationNormalized) => {
  // Finish progress bar


  // set page title
  // document.title = to.meta.title
})
