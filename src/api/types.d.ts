export interface FuncObj111 extends ITabItem {
  id: number,
  funcNum: string,
  funcName: string,
  aliasName: string,
  requestUrl: string,
  belongSystem: string,
  level: number | string,
  funcType: string,
  routePath: string,
  sort: number | string,
  parentFuncNum: string,
  displayTerminal: string
}

export class FuncObj {
  id: number;                 // id
  funcNum: string;            // 菜单号
  funcName: string;           // 菜单名称
  aliasName: string;          // 菜单别名
  requestUrl: string;         // 请求连接
  belongSystem: string;       // 所属系统
  level: number | string;     // 所属层级
  funcType: string;           // 功能类型 MODULE SYSTEM MENU 
  routePath: string;          // 路由路径
  sort: number | string;      // 排序
  parentFuncNum: string;      // 父级节点
  displayTerminal: string;    // 展示终端  
  children: Array<FuncObj>;   // 子集
}

export interface ITabItem {
  getShowText: () => string
}


export interface CascaderOptions { 
  name: string,
  id: string
}

export interface SelectOptions { 
  label: string | number,
  value: string | number
}

export class Organization {
  id: number;                 // id
  orgNo: string;              // 菜单号
  orgName: string;            // 菜单名称
  parentName: string;          // 菜单别名
  parentNo: string;         // 请求连接
  belongSystem: string;       // 所属系统
  level: number | string;     // 所属层级
  funcType: string;           // 功能类型 MODULE SYSTEM MENU 
  routePath: string;          // 路由路径
  sort: number | string;      // 排序
  parentFuncNum: string;      // 父级节点
  displayTerminal: string;    // 展示终端  
  children: Array<FuncObj>;   // 子集
}