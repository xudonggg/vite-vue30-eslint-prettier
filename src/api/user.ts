import request from '@/utils/request';

export function login(params: any) {
    return request({
        url: '/admin/login',
        method: 'post',
        data: params
    })
}

export function getUserInfo() {
    return request({
        url: '/admin/info',
        method: 'get',
    })
}

export function logout() {
    return request({
        url: '/admin/logout',
        method: 'post'
    })
}
