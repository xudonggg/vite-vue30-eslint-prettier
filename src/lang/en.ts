
export default {
  windowFrame: {
    logName: 'Self Random',
    modifyPwd: 'Modify Password',
    loginOut: 'Login Out',
  },
  route: {
    dashboard: 'Home'
  },
  login: {
    title: 'System Login',
    logIn: 'Login',
    username: 'Account',
    password: 'Password',
  },
  errorLog: {
  },
  excel: {
  },
  theme: {
    change: '换肤',
    documentation: '换肤文档',
    tips: 'Tips: 它区别于 navbar 上的 theme-pick, 是两种不同的换肤方法，各自有不同的应用场景，具体请参考文档。'
  },
  settings: {
    title: '系统布局配置',
    theme: '主题色',
    showTagsView: '显示 Tags-View',
    showSidebarLogo: '显示侧边栏 Logo',
    fixedHeader: '固定 Header',
    sidebarTextTheme: '侧边栏文字主题色'
  }
}
