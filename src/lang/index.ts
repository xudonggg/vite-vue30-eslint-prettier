import { createI18n } from 'vue-i18n'
import { getLanguage } from '@/utils/cookies';
import enLocaleEl from 'element-plus/lib/locale/lang/en';
import zhLocaleEl from 'element-plus/lib/locale/lang/zh-cn';
// User defined lang
import enLocale from './en';
import zhLocale from './zh';


const messages = {
  en: {
    ...enLocale,
    el: enLocaleEl.el
  },
  zh: {
    ...zhLocale,
    el: zhLocaleEl.el
  }
};

export const getLocale = () => {
  const cookieLanguage = getLanguage()
  if (cookieLanguage) {
    document.documentElement.lang = cookieLanguage
    return cookieLanguage
  }

  const language = navigator.language.toLowerCase()
  const locales = Object.keys(messages)
  for (const locale of locales) {
    if (language.indexOf(locale) > -1) {
      document.documentElement.lang = locale
      return locale
    }
  }
  // Default language is zh
  return 'zh'
}

const i18n = createI18n({
  locale: getLocale(),
  messages
})

export default i18n
