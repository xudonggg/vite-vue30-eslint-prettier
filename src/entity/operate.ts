export interface Organize {
  id: number,
  createId: string,
  createTime: string,
  level: number,
  orgName: string,
  orgNo: string,
  parentName: string,
  parentNo: string,
  status: number,
  updateId: string,
  updateTime: string,
}


export interface OrganizeTree {
  id: number,
  createId: string,
  createTime: string,
  level: number,
  orgName: string,
  orgNo: string,
  parentName: string,
  parentNo: string,
  status: number,
  updateId: string,
  updateTime: string,
  children: Array<OrganizeTree>
}