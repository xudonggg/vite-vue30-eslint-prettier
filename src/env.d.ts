interface ImportMetaEnv {
  readonly VITE_VUE_APP_BASE_API: string;
  // 更多环境变量...
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
