/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

declare module 'dx-we-ui';


declare module 'china-area-data' {
  const chinaAreaData: any
  export default chinaAreaData
}
declare module 'china-area-data/data-array' {
  const chinaAreaArr: any
  export default chinaAreaArr
}
declare module '../lib/EluiChinaAreaDht.umd.min' {
  const EluiChinaAreaDht: any
  export default EluiChinaAreaDht
}

//declare声明宣告， 声明一个ambient module(即:没有内部实现的 module声明)

declare module "*.vue" {
  import { App, defineComponent } from "vue";
  const component: ReturnType<typeof defineComponent> & {
    install(app: App): void;
  };
  export default component;
}=