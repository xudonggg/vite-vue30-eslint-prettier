import { VuexModule, Module, Mutation, Action, getModule } from 'vuex-module-decorators';
import store from '@/store';


export interface FrameState {
    xHandlerWidth: number;
}

@Module({ dynamic: true, store, name: 'frame' })
class Frame extends VuexModule implements FrameState{

    public xHandlerWidth: number = 0;

    @Mutation
    private HANDLER_WIDTH_CHANGE(width: number) {
        this.xHandlerWidth = width;
    }

    @Action
    public setXHandlerWidth(width: number) {
        this.HANDLER_WIDTH_CHANGE(width)
    }

}

export const FrameModule = getModule(Frame);
