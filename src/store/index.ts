
import { AppState } from './modules/app';
import {FrameState} from './modules/frame';

import Vuex from 'vuex';

export interface RootState {
  app: AppState,
  frame: FrameState,
  
}

// Declare empty store first, dynamically register all modules later.
export default new Vuex.Store<RootState>({})
