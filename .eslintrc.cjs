module.exports = {
  env: {
    es2021: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:vue/vue3-essential',
    'plugin:@typescript-eslint/recommended',
    'plugin:vue/vue3-recommended',
    'plugin:prettier/recommended',
  ],
  overrides: [],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: ['vue', '@typescript-eslint', 'prettier'],
  rules: {
    indent: ['error', 'tab'],
    'linebreak-style': ['error', 'unix'],
    quotes: ['error', 'single'],
    semi: ['error', 'never'],
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    // allow async-await
    'generator-star-spacing': 'off',
    'no-trailing-spaces': 'off',
    indent: 'off',
    'spaced-comment': 'off',
    'no-trailing-spaces': 'off',
    'no-new': 'off',
    'key-spacing': 'off',
    'comma-dangle': 'off',
    'eol-last': 'off',
    quotes: 'off',
    semi: 'off',
    'keyword-spacing': 'off',
    'space-before-blocks': 'off',
    'comma-spacing': 'off',
    'space-before-function-paren': 'off',
    'space-infix-ops': 'off',
    'no-multiple-empty-lines': 'off',
    'block-spacing': 'off',
    'padded-blocks': 'off',
    'arrow-spacing': 'off',
    'no-sequences': 'off',
    'no-unused-expressions': 'off',
    'no-multi-spaces': 'off',
    'no-unused-vars': 'off',
    'camelcase ': 'off',
    '@typescript-eslint/no-inferrable-types': 'off',
    'prettier/prettier': 'off',
    '@typescript-eslint/no-empty-function': 'off',
    '@typescript-eslint/no-unused-vars': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    'vue/no-unused-vars': 'off',
    'prefer-const': 'off',
    'vue/no-unused-components': 'off',
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': ['error'],
    '@typescript-eslint/interface-name-prefix': ['error', { prefixWithI: 'always' }],
  },
}
