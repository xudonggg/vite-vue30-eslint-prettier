import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { resolve } from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    // 路径别名配置
    alias: {
      "@": resolve(__dirname, "./src"),
      "@assets": resolve(__dirname, "./src/assets"),
      "@common": resolve(__dirname, "./src/common"),
      "@interface": resolve(__dirname, "./src/interface"),
      "@plugins": resolve(__dirname, "./src/plugins"),
      "@utils": resolve(__dirname, "./src/utils"),
      "@components": resolve(__dirname, "./src/components"),
      "@styles": resolve(__dirname, "./src/styles"),
      "@store": resolve(__dirname, "./src/store"),
      "@views": resolve(__dirname, "./src/views"),
    },
  },
  /* 打包 */
  build: {
    outDir: "leoMedicineTq",
    //是否生成source map 源码文件
    sourcemap: false,
    terserOptions: {
      //生产环境移除 console
      compress: {
        drop_console: false,
        drop_debugger: true,
      },
    },
  },
  //服务配置
  server: {
    open: true,
    //切换到生产环境需要将host注释
    // host: "test.daka.com",
    hmr:true, //开启热更新
    host: "localhost",
    port: 8082,
    proxy: {
      '/leo-medicine-bcompany': {
        //测试环境地址
        // target: "http://172.17.47.100/",
        //生产环境地址
        target: 'http://localhost:8201', // 测试环境
        ws: true,
        changeOrigin: true
      }
    }
  }
});
